from apps.factory import create_app
from apps.db import db 
from apps.security import bcrypt
from getpass import getpass

app = create_app()
app.app_context().push()

try:
    db.table_relationship.drop()
    db.import_template.drop()
    db.form_permission.drop()
    db.field_permission.drop()
    db.rg_permission.drop()
    db.project.drop()
    db.form.drop()
    db.rule.drop()
    db.sequence.drop()
    db.sessions.drop()
    db.table_metadata.drop()
    db.data_upload_log.drop()
    db.columns_info.drop()
    db.action_history.drop()
    db.rule_group.drop()
    db.pdb_user.drop()

    # create some default tables 
    db.create_collection('table_relationship')
    db.create_collection('import_template')
    db.create_collection('form_permission')
    db.create_collection('field_permission')
    db.create_collection('rg_permission')
    db.create_collection('project')
    db.create_collection('form')
    db.create_collection('rule')
    db.create_collection('sequence')
    db.create_collection('sessions')
    db.create_collection('table_metadata')
    db.create_collection('data_upload_log')
    db.create_collection('columns_info')
    db.create_collection('action_history')
    db.create_collection('rule_group')
    
    db.action_history.create_index([
        ('table_name', 1),
        ('key_id', 1),
        ('field', 1)
    ])

    # insert data types
    data_types = [{'name': 'Text'}, {'name': 'Number'}, {'name': 'Date'}, {'name': 'Datetime'}, {'name': 'Dropdown'}, {'name': 'Checkbox'}, {'name': 'File'}, {'name': 'Table'}, {'name': 'Field Group'}, {'name': 'Relation'}]
    db.data_type.insert_many(data_types)
    print('Insert data types --> Done')

    # insert rule group
    db.rule_group.insert_one({'name': 'default'})
    print('Insert default --> Done')

    # add role admin 
    db.role.insert_one({'name': 'admin'})
    print('Create role admin --> Done')

    # create admin
    print('Enter admin information')
    username = str(input('username: '))
    email = str(input('email: '))
    name = str(input('name: '))

    password = getpass('password: ')
    confirm_password = getpass('enter password again to confirm: ')
    while password != confirm_password:
        print('password does not match, enter again')
        password = getpass('password: ')
        confirm_password = getpass('enter password again to confirm: ')
    
    roles = ['admin']
    password_hash = bcrypt.generate_password_hash(password=password.encode('utf8').decode('utf-8'))

    db.pdb_user.insert_one({'username': username, 'email': email, 'name': name, 'password_hash': password_hash, 'roles': roles})
    print('Create new admin user --> Done')

    # create record_group default
    db.record_group.insert_one({'name': 'default'})
    print('Create record group default --> Done')
except Exception as e:
    print(str(e))
    print('Internal error, please try running init script again.')