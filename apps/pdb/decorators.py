from functools import wraps
from bson.objectid import ObjectId
from flask import jsonify
from flask_jwt_extended import verify_jwt_in_request, get_jwt_claims, get_jwt_identity

from apps.db import db


def admin_required(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        current_user = get_jwt_identity()
        user_id = ObjectId(current_user['_id'])

        user = db.pdb_user.find_one({'_id': user_id})
        user_roles = user['roles']
        if 'admin' not in user_roles:
            return jsonify({'msg': 'Admin required'}), 403
        else:
            return fn(*args, **kwargs)
    return wrapper


def is_pdb_user(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        pass 

    return wrapper 