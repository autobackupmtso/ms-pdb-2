from flask import Blueprint, Response, request, jsonify, make_response, current_app
from flask_cors import CORS
from flask_jwt_extended import jwt_required, get_jwt_identity

from .db_interaction.export import *

from apps.pdb.decorators import admin_required

export_api = Blueprint('export_api', 'export_api', url_prefix='/api/pdb/export')
CORS(export_api)


@export_api.route('/get/<_id>', methods=['GET'])
@jwt_required
def api_get_export(_id):
    export = get_export(_id)
    return export


@export_api.route('/get-all', methods=['GET'])
@jwt_required
def api_get_all():
    exports = get_all()
    return jsonify(exports)


@export_api.route('/facet-search', methods=['POST'])
@jwt_required
def api_facet_search():
    condition = request.get_json()
    exports = facet_search(condition)
    return jsonify(exports)


@export_api.route('/add', methods=['POST'])
@jwt_required
def api_add_export():
    inp = request.get_json()

    resp = add_export(inp)
    if isinstance(resp, dict):
        return jsonify({'msg': resp['err']}), 400

    return jsonify({'_id': resp.inserted_id})


@export_api.route('/update', methods=['PUT'])
@jwt_required
def api_update_export():
    inp = request.get_json()
    if '_id' not in inp:
        return jsonify({'msg': '_id is missing'}), 400

    resp = update_export(inp)
    return jsonify({'modified': resp.modified_count})


@export_api.route('/delete/<_id>', methods=['DELETE'])
@jwt_required
def api_delete_export(_id):
    resp = delete_export(_id)
    return jsonify({'deleted': resp.deleted_count})


@export_api.route('/run', methods=['POST'])
@jwt_required
def api_run_export():
    inp = request.get_json()
    template_id = inp['template_id']
    use_template = inp.get('use_template', False)

    current_user = get_jwt_identity()
    user_id = current_user['_id']

    export, file_name, extension = run_export(template_id, user_id, use_template)

    output = make_response(export)
    output.headers["Content-Disposition"] = "attachment; filename=" + file_name
    output.headers["Access-Control-Expose-Headers"] = 'Content-Disposition'

    if extension == 'xlsm':
        output.headers["Content-type"] = 'application/vnd.ms-excel.sheet.macroEnabled.12'
    else:
        output.headers["Content-type"] = 'application/octet-stream'
    return output
