from apps.db import db 
from bson.objectid import ObjectId

def get_project(_id, user_id):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    user_roles = db.role.find({'name': {'$in': roles_name}})
    role_ids = [r['_id'] for r in user_roles]

    if 'admin' in roles_name:
        return db.project.find_one({'_id': ObjectId(_id)})
    else:
        pipeline = [
            {
                '$lookup': {
                    'from': 'form_permission', 
                    'localField': '_id', 
                    'foreignField': 'project_id', 
                    'as': 'p'
                }
            }, {
                '$match': {
                    '_id': ObjectId(_id),
                    'p': {
                        '$elemMatch': {
                            'form_permission': True, 
                            'role_id': {'$in': role_ids}
                        }
                    }
                }
            }, {
                '$project': {
                    'p': 0
                }
            }
        ]
        res = list(db.project.aggregate(pipeline))

        if len(res) != 0:
            return res[0]
        else:
            return {}

def get_all_project(user_id):
    user_id = ObjectId(user_id)
    roles_name = db.pdb_user.find_one({'_id': user_id})['roles']
    user_roles = db.role.find({'name': {'$in': roles_name}})
    role_ids = [r['_id'] for r in user_roles]

    if 'admin' in roles_name:
        return list(db.project.find())
    else:        
        pipeline = [
            {
                '$lookup': {
                    'from': 'form_permission', 
                    'localField': '_id', 
                    'foreignField': 'project_id', 
                    'as': 'p'
                }
            }, {
                '$match': {
                    'p': {
                        '$elemMatch': {
                            'form_permission': True, 
                            'role_id': {'$in': role_ids}
                        }
                    }
                }
            }, {
                '$project': {
                    'p': 0
                }
            }
        ]

        return list(db.project.aggregate(pipeline))

def add_project(inp):
    resp = db.project.insert_one(inp)
    return resp.inserted_id

# def add_form_by_xlsx(project_id, form_name, inp):
#     data_types = ['Text', 'Number', 'Date', 'Datetime', 'Dropdown', 'Checkbox', 'File', 'Table', 'Field Group']

#     form_prop = {}
#     err_msgs = []

#     metadata = db.project.find({'_id': ObjectId(project_id)}).next()['metadata']

#     for id in range(len(inp)):
#         field_name = str(inp[id][0]).strip()
#         if field_name in form_prop:
#             # line_id = '%d' % id + 2
#             # err = 'Field %s duplicated' % field_name
#             msg = 'Line %d: field %s duplicated'  % (id + 2, field_name)
#             # err_msgs.append([line_id, err])
#             err_msgs.append(msg)
#             continue
        
#         if field_name == '':
#             # line_id = '%d' % id + 2
#             # err = 'Field must not be blank'
#             msg = 'Line %d: field name must not be blank'  % (id + 2)
#             # err_msgs.append([line_id, err])
#             err_msgs.append(msg)
#             continue

#         field_type = str(inp[id][1]).strip()
#         if field_type not in data_types:
#             # line_id = id + 2
#             # err = 'Type \'%s\' is not in data types (%s)' % (field_type, str(data_types)[1:-1])
#             msg = 'Line %d: Type \'%s\' is not in data types (%s)'  % (id + 2, field_type, str(data_types)[1:-1])
#             # err_msgs.append([line_id, err])
#             err_msgs.append(msg)
#             continue

#         form_prop[field_name] = {'sort_id': id, 'type': field_type}

#         if field_type == 'Dropdown' and len(inp[id]) >= 3:
#             dropdown_list = [s.strip() for s in str(inp[id][2]).split('|-|')]
#             form_prop[field_name]['values'] = dropdown_list
#         if field_type == 'Checkbox' and len(inp[id]) >= 3:
#             checkbox_string = str(inp[id][2]).strip()
#             form_prop[field_name]['values'] = checkbox_string

#     metadata[form_name] = form_prop
#     if len(err_msgs) == 0:
#         resp = db.project.update_one({'_id': ObjectId(project_id)}, {'$set': {'metadata': metadata}})
#     else:
#         resp = None
#     return resp, err_msgs


def update_project(_id, props):
    resp = db.project.update_one({'_id': ObjectId(_id)}, {'$set': props})
    return resp 

def delete_project(_id):
    project_resp = db.project.delete_one({'_id': ObjectId(_id)})
    data_resp = db.data.delete_many({'project_id': ObjectId(_id)})
    return project_resp, data_resp

# def add_form(_id, form_name, form_info):
#     _id = ObjectId(_id)
#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}

#     project_metadata = project['metadata']
#     if form_name in project_metadata:
#         return {'err': 'Form already exists'}

#     resp = db.project.update_one({'_id': _id}, {'$set': {'metadata.' + form_name: form_info}})
#     return resp

# def update_form_name(_id, old_name, new_name):
#     _id = ObjectId(_id)
#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}, None

#     project_metadata = project['metadata']
#     if old_name not in project_metadata:
#         return {'err': 'Form does not exist'} , None       

#     old_form_name = 'metadata.' + old_name
#     new_form_name = 'metadata.' + new_name
#     resp1 = db.project.update_one({'_id': _id}, {'$rename': {old_form_name: new_form_name}})
#     resp2 = db.data.update_many({'project_id': _id, 'form_name': old_name}, {'$set': {'form_name': new_name}})
#     return resp1, resp2

# def delete_form(_id, form_name):
#     _id = ObjectId(_id)
#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}, None

#     project_metadata = project['metadata']
#     if form_name not in project_metadata:
#         return {'err': 'Form does not exist'}, None

#     resp1 = db.project.update_one({'_id': _id}, {'$unset': {'metadata.' + form_name: ""}})
#     resp2 = db.data.delete_many({'project_id': _id, 'form_name': form_name})
#     return resp1, resp2

# def add_field(_id, form_name, field_name, field_info):
#     _id = ObjectId(_id) 

#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}

#     project_metadata = project['metadata']
#     if form_name not in project_metadata:
#         return {'err': 'Form does not exist'}
    
#     if field_name in project_metadata[form_name]:
#         return {'err': 'Field already exists'}

#     resp = db.project.update_one({'_id': _id}, {'$set': {'metadata.' + form_name + '.' + field_name: field_info}})
#     return resp

# def add_many_fields(_id, form_name, fields: dict):
#     _id = ObjectId(_id) 

#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}

#     project_metadata = project['metadata']
#     if form_name not in project_metadata:
#         return {'err': 'Form does not exist'}

#     err = []

#     for f in fields:
#         if f in project_metadata[form_name]:
#             msg = '%s already exists' % f
#             err.append(msg)

#     if len(err) != 0:
#         return err 

#     project_metadata[form_name].update(fields)

#     resp = db.project.update_one({'_id': _id}, {'$set': {'metadata': project_metadata}})
#     return resp
    

# def update_field_name(_id, form_name, old_name, new_name):
#     _id = ObjectId(_id)

#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}, None

#     project_metadata = project['metadata']
#     if form_name not in project_metadata:
#         return {'err': 'Form does not exist'}, None

#     if old_name not in project_metadata[form_name]:
#         return {'err': 'Field does not exist'}, None

#     old_chain = 'metadata.' + form_name + '.' + old_name
#     new_chain = 'metadata.' + form_name + '.' + new_name

#     resp1 = db.project.update_one({'_id': _id}, {'$rename': {old_chain: new_chain}})
#     resp2 = db.data.update_many({'project_id': _id, 'form_name': form_name}, {'$rename': {old_name: new_name}})
#     return resp1, resp2

# def update_field_type():
#     pass 

# def delete_field(_id, form_name, field_name):
#     _id = ObjectId(_id)

#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}, None

#     project_metadata = project['metadata']
#     if form_name not in project_metadata:
#         return {'err': 'Form does not exist'}, None

#     resp1 = db.project.update_one({'_id': _id}, {'$unset': {'metadata.' + form_name + '.' + field_name: ""}})
#     resp2 = db.data.update_many({'project_id': _id, 'form_name': form_name}, {'$unset': {field_name: ""}})
#     return resp1, resp2
    

# def delete_many_fields(_id, form_name, fields):
#     _id = ObjectId(_id)

#     project = db.project.find_one({'_id': _id}) 

#     if project is None:
#         return {'err': 'Project does not exist'}, None

#     project_metadata = project['metadata']
#     if form_name not in project_metadata:
#         return {'err': 'Form does not exist'}, None

#     project_delete_info = {}
#     data_delete_info = {}
#     for f in fields:
#         project_delete_info['metadata.' + form_name + '.' + f] =  ""
#         data_delete_info[f] = ""

#     resp1 = db.project.update_one({'_id': _id}, {'$unset': project_delete_info})
#     resp2 = db.data.update_many({'project_id': _id, 'form_name': form_name}, {'$unset': data_delete_info})
#     return resp1, resp2 

# def delete_many_fields_by_xlsx():
#     pass 