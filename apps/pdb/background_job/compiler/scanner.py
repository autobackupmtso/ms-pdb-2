import math

key_words = ['abs', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atanh', 'avg', 'avgFields', 'ceil', 'clearField',
             'concat', 'cos', 'cosh', 'countFields', 'currentDate', 'currentDateTime', 'dayOfMonth', 'decimals', 'exp',
             'floor', 'formLink', 'getDays', 'getRecordName', 'getFieldLabel', 'getUserMail', 'hasRole', 'hour', 'if',
             'lastFieldModifiedDate', 'lastFieldModifiedName', 'lastFieldModifiedUserName', 'lastRecordModifiedDate',
             'lastRecordModifiedName', 'lastRecordModifiedUserName', 'length', 'ln', 'log', 'match', 'max', 'maxFields',
             'median', 'medianFields', 'min', 'minFields', 'minute', 'mod', 'month', 'padLeft', 'padRight', 'pathValue',
             'rand', 'relatedValue', 'replace', 'round', 'second', 'sin', 'sinh', 'sqrt', 'str', 'subString', 'sum',
             'sumFields', 'tan', 'tanh', 'toDatePattern', 'toDateString', 'toDatetimeString', 'toLower', 'toUpper',
             'week', 'year', 'day', 'value']

basic_num_func = ['abs', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atanh', 'ceil', 'cos', 'cosh', 'exp', 'floor', 'ln',
                  'log', 'round', 'sin', 'sinh', 'sqrt', 'tan', 'tanh']
non_param_func = []
token_types = ['+', '-', '*', '/', '%', '=', '<>', '<', '<=', '>', '>=', '(', ')', ',', "'", ':', '.']
cmp_tokens = ['=', '<>', '<', '<=', '>', '>=']


def get_ch(current_pos: int, string):
    if current_pos + 1 >= len(string):
        return current_pos, None
    else:
        return current_pos + 1, string[current_pos + 1]


def get_token(current_pos, string):
    c = ''

    while c == '' or c == ' ' or c == '\n':
        # while c == '' or not c.isalnum() or c not in token_types:
        current_pos, c = get_ch(current_pos, string)

    if c is None:
        return current_pos, None

    elif c.isalpha():
        tmp = ''
        while c is not None and c.isalnum():
            tmp += c
            current_pos, c = get_ch(current_pos, string)

        if c is None:
            return current_pos, tmp
        elif not c.isalnum():
            current_pos -= 1

        return current_pos, tmp

    elif c.isdigit():
        tmp = ''
        while c is not None and c.isdigit():
            tmp += c
            current_pos, c = get_ch(current_pos, string)

        if c == '.':
            tmp += c
            current_pos, c = get_ch(current_pos, string)
            while c is not None and c.isalnum():
                tmp += c
                current_pos, c = get_ch(current_pos, string)

        if c is None:
            return current_pos, tmp
        elif not c.isdigit():
            current_pos -= 1

        return current_pos, tmp

    elif c in token_types:
        # if c == '-':
        #     current_pos, c = get_ch(current_pos, string)
        #     if c.isdigit():
        #         tmp = ''
        #         while c is not None and c.isdigit():
        #             tmp += c
        #             current_pos, c = get_ch(current_pos, string)
        #
        #         if c == '.':
        #             tmp += c
        #             current_pos, c = get_ch(current_pos, string)
        #             while c is not None and c.isalnum():
        #                 tmp += c
        #                 current_pos, c = get_ch(current_pos, string)
        #
        #         if c is None:
        #             return current_pos, tmp
        #         elif not c.isdigit():
        #             current_pos -= 1
        #
        #         return current_pos, '-' + tmp
        #     elif c == ' ':
        #         return current_pos, '-'
        #     else:
        #         current_pos -= 1
        #         return current_pos, '-'

        if c == '<':
            current_pos, next_ch = get_ch(current_pos, string)
            if next_ch == '=' or next_ch == '>':
                return current_pos, c + next_ch
            else:
                return current_pos - 1, c
        elif c == '>':
            current_pos, next_ch = get_ch(current_pos, string)
            if next_ch == '=':
                return current_pos, c + next_ch
            else:
                return current_pos - 1, c
        elif c == "'":
            if "'" not in string[current_pos + 1:]:
                raise Exception("' is missing")
                # return current_pos, None
            else:
                text = ''
                current_pos, c = get_ch(current_pos, string)
                while c != "'":
                    text += c
                    current_pos, c = get_ch(current_pos, string)

                return current_pos, "'%s'" % text

        else:
            return current_pos, c

    else:
        return current_pos, None


def scan(string):
    list_tokens = []
    current_pos, token = get_token(-1, string)
    while token is not None:
        list_tokens.append(token)
        current_pos, token = get_token(current_pos, string)
    if current_pos != len(string) - 1:
        return 'Error string'
    return list_tokens


if __name__ == "__main__":
    text = "if(floor(-1.9) > -123 :, 0, 1)"
    # text = 'abs(value("c"))'
    # text = ''
    lst = scan(text)
    print(lst)
