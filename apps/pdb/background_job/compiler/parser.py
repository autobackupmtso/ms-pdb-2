from bson.objectid import ObjectId
from .scanner import scan, key_words, token_types, cmp_tokens, basic_num_func

from apps.db import db


def is_number(param):
    try:
        float(str(param))
        is_float = True
    except ValueError:
        is_float = False

    try:
        int(str(param))
        is_int = True
    except ValueError:
        is_int = False

    return is_float or is_int


def is_text(param: str):
    if param is None:
        return False
    else:
        return param[0] == "'" and param[-1] == "'"


def get_next_token(current_pos, list_token):
    if current_pos + 1 >= len(list_token):
        return current_pos + 1, None
    else:
        return current_pos + 1, list_token[current_pos + 1]


def factor(current_pos: int, list_token: list, table_metadata: dict):
    current_pos, token = get_next_token(current_pos, list_token)
    if is_number(token):
        data_type = 'Number'
        return current_pos, data_type

    elif is_text(token):
        return current_pos, 'Text'

    elif token in ['+', '-']:
        current_pos, data_type = expression(current_pos-1, list_token, table_metadata)
        return current_pos, data_type

    elif token == '(':
        current_pos, data_type = expression(current_pos, list_token, table_metadata)
        if data_type != 'Number':
            raise Exception('( must be followed by number')

        current_pos, token = get_next_token(current_pos, list_token)
        if token == ')':
            return current_pos, 'Number'
        else:
            raise Exception(') is missing')

    elif token in key_words:
        if token in basic_num_func:
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Number':
                raise Exception('param data type must be number')
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, 'Number'

        elif token in ['avg', 'max', 'median', 'min', 'sum']:
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Number':
                raise Exception('param data type must be number')

            current_pos, token = get_next_token(current_pos, list_token)
            if token == ')':
                return current_pos, 'Number'
            elif token == ',':
                while token == ',':
                    current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
                    if param_data_type != 'Number':
                        raise Exception('param is missing or data type is not numeric')
                    current_pos, token = get_next_token(current_pos, list_token)

                if token == ')':
                    return current_pos, 'Number'

                elif not is_number(token):
                    raise Exception('param data type must be number')

                current_pos, token = get_next_token(current_pos, list_token)
                if token != ')':
                    raise Exception(') is missing')

                return current_pos, 'Number'
            else:
                raise Exception(') is missing')

        elif token in ['countFields', 'avgFields', 'maxFields', 'minFields', 'medianFields', 'sumFields']:
            # need to check data type of field
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param data type must be text')
            ###
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param data type must be text')
            ###
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, 'Number'

        elif token == 'clearField':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'null'

        elif token == 'concat':
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param data type must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token == ')':
                return current_pos, 'Text'
            elif token == ',':
                while token == ',':
                    current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
                    if param_data_type != 'Text':
                        raise Exception('param is missing or data type is not text')
                    current_pos, token = get_next_token(current_pos, list_token)

                if token == ')':
                    return current_pos, 'Text'

                elif not is_number(token):
                    raise Exception('param data type must be text')

                current_pos, token = get_next_token(current_pos, list_token)
                if token != ')':
                    raise Exception(') is missing')

                return current_pos, 'Text'
            else:
                raise Exception(') is missing')

        elif token == 'currentDate':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Date'

        elif token == 'currentDateTime':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Datetime'

        elif token == 'getRecordName':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Text'

        elif token in ['hour', 'minute', 'second']:
            # get hour, minute, second from datetime
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Datetime':
                raise Exception('param data type must be Datetime')
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, 'Number'

        elif token in ['year', 'month', 'day']:
            # get hour, minute, second from datetime
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type not in ['Datetime', 'Date']:
                raise Exception('param data type must be Date or Datetime')
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, 'Number'

        elif token == 'if':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, _ = expression(current_pos, list_token, table_metadata)
            current_pos, cmp_token = get_next_token(current_pos, list_token)

            if cmp_token not in cmp_tokens:
                raise Exception('compare token is missing')

            current_pos, _ = expression(current_pos, list_token, table_metadata)

            current_pos, token = get_next_token(current_pos, list_token)

            if token != ',':
                raise Exception(', is missing')

            current_pos, param1_data_type = expression(current_pos, list_token, table_metadata)

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param2_data_type = expression(current_pos, list_token, table_metadata)

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            if param1_data_type != param2_data_type:
                raise Exception('data type of param1 and param2 must be the same')

            return current_pos, param1_data_type

        elif token == 'lastFieldModifiedDate':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = factor(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('field name must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Date'

        elif token == 'lastRecordModifiedDate':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Date'

        elif token in ['lastRecordModifiedName', 'lastRecordModifiedUserName']:
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Text'

        elif token in ['lastFieldModifiedName', 'lastFieldModifiedUserName']:
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = factor(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('field name must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Date'

        elif token in ['length', 'toLower', 'toUpper']:
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param data type must be text')
            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, 'Text'

        elif token in ['padLeft', 'padRight', 'subString']:
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = factor(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param data type must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = factor(current_pos, list_token, table_metadata)
            if param_data_type != 'Number':
                raise Exception('param2 in function must be number')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = factor(current_pos, list_token, table_metadata)
            if param_data_type != 'Number':
                raise Exception('param3 in function must be number')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Text'

        elif token == 'rand':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Number'

        elif token == 'replace':
            current_pos, token = get_next_token(current_pos, list_token)
            if token != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param data type must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param2 in function must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ',':
                raise Exception(', is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)
            if param_data_type != 'Text':
                raise Exception('param3 in function must be text')

            current_pos, token = get_next_token(current_pos, list_token)
            if token != ')':
                raise Exception(') is missing')

            return current_pos, 'Text'

        elif token == 'str':
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, param_data_type = expression(current_pos, list_token, table_metadata)

            current_pos, token2 = get_next_token(current_pos, list_token)

            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, 'Text'

        elif token == 'value':
            current_pos, token1 = get_next_token(current_pos, list_token)
            if token1 != '(':
                raise Exception('( is missing')

            current_pos, field_name = get_next_token(current_pos, list_token)
            field_name = field_name[1:-1]
            if field_name not in table_metadata:
                raise Exception('field does not exist')

            current_pos, token2 = get_next_token(current_pos, list_token)
            if token2 != ')':
                raise Exception(') is missing')

            return current_pos, table_metadata[field_name]['type']

    else:
        return current_pos, 'Unknown'


def term(current_pos, list_token, table_metadata):
    current_pos, data_type = factor(current_pos, list_token, table_metadata)

    if data_type != 'Number':
        return current_pos, data_type

    current_pos, token = get_next_token(current_pos, list_token)
    while token in ['*', '/']:
        current_pos, data_type = factor(current_pos, list_token, table_metadata)

        if data_type != 'Number':
            raise Exception('param data type must be number')

        current_pos, token = get_next_token(current_pos, list_token)

    return current_pos - 1, 'Number'


def expression(current_pos, list_token, table_metadata):
    current_pos, token = get_next_token(current_pos, list_token)

    if token in ['+', '-']:
        current_pos, data_type = term(current_pos, list_token, table_metadata)
    else:
        current_pos, data_type = term(current_pos - 1, list_token, table_metadata)
        if data_type != 'Number':
            return current_pos, data_type

    current_pos, token = get_next_token(current_pos, list_token)
    while token in ['+', '-']:
        current_pos, data_type = term(current_pos, list_token, table_metadata)

        if data_type != 'Number':
            raise Exception('param data type must be number')

        current_pos, token = get_next_token(current_pos, list_token)

    return current_pos - 1, 'Number'


def validate_function(table_id, function):
    table_id = ObjectId(table_id)
    table = db.table_metadata.find_one({'_id': table_id})
    table_metadata = table['metadata']
    list_token = scan(function)

    resp = {'result': '', 'err': ''}

    try:
        pos, data_type = expression(-1, list_token, table_metadata)
        if pos < len(list_token) - 1:
            err_msg = 'Redundant text: %s' % ' '.join(list_token[pos+1:])
            resp['result'] = 'fail'
            resp['err'] = err_msg
            return resp
        else:
            resp['result'] = 'success'
            resp['err'] = ''
            return resp

    except Exception as e:
        resp['result'] = 'fail'
        resp['err'] = str(e)
        return resp


if __name__ == "__main__":
    table_metadata = {'a': {'type': 'Text'}}
    record = {'a': 'nguyen duy manh'}
    texts = []
    # texts.append('abs(acos(1/2))')
    # texts.append('if( abs(-9) < 0, 0, 1)')
    # texts.append("if(avg(1,2,3) < 10, abs(-1.5), 1)")
    # texts.append("if('a' = 'a' , 'True', 'False   ')")
    # texts.append('-acos(-9)')
    texts.append('4*5/')
    # texts.append('-abs(-9)')
    # texts.append('clearField())')
    # texts.append("concat('1', '2', '3')")
    # texts.append('currentDate)')
    # texts.append('getRecordName()')
    # texts.append('lastFieldModifiedDate()')
    # texts.append('length(currentDate())')
    # texts.append("padLeft('duymanh', 4, 1)")
    # texts.append('rand()')
    # texts.append("replace(value('b'), 'a', 'b')")
    # texts.append('sum(1, 2, floor(3.5))')
    # texts.append("month(currentDate())")
    # texts.append("if ('a' = 'b', floor(1.5), round(1.5))")
    # texts.append("toUpper('aBcD')")
    # texts.append('avg(1, 2, )')

    for text in texts:
        list_token = scan(text)
        print(list_token)

        try:
            pos, data_type = expression(-1, list_token, table_metadata)
            if pos < len(list_token) - 1:
                err_msg = 'Redundant text: %s' % ' '.join(list_token[pos + 1:])
                print(err_msg)
            print(pos, data_type)
            print()
        except Exception as e:
            print(str(e))
            print()