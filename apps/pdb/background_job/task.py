from flask_mail import Message
from flask import current_app, copy_current_request_context

from apps.db import db
from apps.pdb.background_job.db_rule import exec_rule
from apps.pdb.models.db_interaction.data_filter import convert_filter

from bson.objectid import ObjectId
from threading import Thread  # for test, will be replaced by Celery


def task_controller(sender, msg, **kwargs):
    msg_list = ['insert_one', 'insert_many', 'update_one', 'update_many']

    if msg == 'insert_one':
        table_name = kwargs['table_name']
        inserted_id = kwargs['inserted_id']
        trigger_fields = kwargs['trigger_fields']
        check_matching_rule(table_name, trigger_fields, [inserted_id])
        test_send_mail(table_name, trigger_fields, inserted_id)
    elif msg == 'insert_many':
        table_name = kwargs['table_name']
        inserted_ids = kwargs['inserted_ids']
        trigger_fields = kwargs['trigger_fields']
        check_matching_rule(table_name, trigger_fields, inserted_ids)
    elif msg == 'update_one':
        table_name = kwargs['table_name']
        modified_id = kwargs['modified_id']
        trigger_fields = kwargs['trigger_fields']
        check_matching_rule(table_name, trigger_fields, [modified_id])
    elif msg == 'update_many':
        print(kwargs)
        table_name = kwargs['table_name']
        trigger_fields = kwargs['trigger_fields']
        modified_ids = kwargs['modified_ids']
        check_matching_rule(table_name, trigger_fields, modified_ids)


def check_matching_rule(table_name, record_ids: [], created_fields, updated_fields, deleted_fields, record_flag):
    # table_id = ObjectId(table_id)

    rules = list(db.rule.find({'trigger_table': table_name, 'trigger_field': {'$in': created_fields+updated_fields+deleted_fields}}))
    for rule in rules:
        _id = rule['_id']
        additional_conditions = {}
        if record_ids is not None:
            obj_ids = [ObjectId(record_id) for record_id in record_ids]
            additional_conditions['_id'] = {'$in': obj_ids}

        exec_rule(_id, created_fields, updated_fields, deleted_fields, record_flag, additional_conditions)


def test_send_mail(table_name, trigger_fields, record_id):
    mail = current_app.mail
    rules = list(db.rule.find({'trigger_table': table_name, 'trigger_field': {'$in': trigger_fields}, 'action': 'Send Mail'}))
    
    for rule in rules:
        condition = rule['condition']
        subject = rule['subject']
        recipients = rule['recipients']
        body = rule['content']
        subject_params = rule['subject_field']
        body_params = rule['content_field']

        mongo_cond = convert_filter(condition)
        data = db[table_name].find_one({'_id': ObjectId(record_id), **mongo_cond})
        if data is None or data == {}:
            continue

        subject_params_values = tuple([data.get(p, '') for p in subject_params])
        body_params_values = tuple([data.get(p, '') for p in body_params])

        static_addresses = [add['name'] for add in recipients if add['type'] == 'Static Address']
        users = [add['email'] for add in recipients if add['type'] == 'User']
        roles = [add['name'] for add in recipients if add['type'] == 'Role']

        db_users = list(db.pdb_user.find({
            '$or': [
                {'email': {'$in': users}},
                {'roles': {'$in': roles}}
            ]            
        }))
        db_emails = [d['email'] for d in db_users]
        recipients = static_addresses + db_emails
        
        subject = subject % subject_params_values
        body = body % body_params_values

        msg = Message(subject=subject, recipients=recipients)
        msg.html = body

        @copy_current_request_context
        def send_async_email(app, mail, msg):
            with app.app_context():
                mail.send(msg)

        Thread(target=send_async_email, args=(current_app, mail, msg)).start()
