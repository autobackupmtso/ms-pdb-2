from flask import Blueprint, request, jsonify
from flask_cors import CORS

from bson.objectid import ObjectId

from .db_interaction.record_group_permission import get_rg_permission, get_all, facet_search, add_rg_permission, add_many, update_rg_permission, upsert_many, delete_rg_permission
from apps.pdb.decorators import admin_required

rg_permission_api = Blueprint('rg_permission_api', 'rg_permission_api', url_prefix='/api/pdb/rg-permission')
CORS(rg_permission_api)

@rg_permission_api.route('/get/<id>', methods=['GET'])
@admin_required
def api_get_rg_permission(id):
    rg_permission = get_rg_permission(id)
    return rg_permission

@rg_permission_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    groups = get_all()
    return jsonify(groups)

@rg_permission_api.route('/facet-search', methods=['POST'])
@admin_required
def api_facet_search():
    condition = request.get_json()

    rg_permissions = facet_search(condition)
    return jsonify(rg_permissions)

@rg_permission_api.route('/add', methods=['POST'])
@admin_required
def api_add_rg_permission():
    inp = request.get_json()
    if 'record_group_id' not in inp     \
        or 'role_id' not in inp         \
        or 'S' not in inp               \
        or 'U' not in inp               \
        or 'I' not in inp               \
        or 'D' not in inp:
        return jsonify({'msg': 'Some fields are missing'}), 400 

    inserted_id = add_rg_permission(inp)
    return jsonify({'_id': inserted_id}) 

@rg_permission_api.route('/add-many', methods=['POST'])
@admin_required
def api_add_many():
    inp = request.get_json()
    for d in inp:
        if 'record_group_id' not in d     \
            or 'role_id' not in d         \
            or 'S' not in d               \
            or 'U' not in d               \
            or 'I' not in d               \
            or 'D' not in d:
            return jsonify({'msg': 'Some fields are missing'}), 400 
        
    resp = add_many(inp)
    return jsonify({'_ids': resp.inserted_ids})

@rg_permission_api.route('/update', methods=['PUT'])
@admin_required
def api_update_rg_permission():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400 

    update_rg_permission(inp)
    return jsonify({'msg': 'success'})

@rg_permission_api.route('/upsert-many', methods=['PUT'])
@admin_required
def api_upsert_many():
    try:
        inp = request.get_json() 
    except Exception as e:
        return jsonify({'msg': str(e)}), 400

    resp = upsert_many(inp)
    if resp != None:
        return jsonify({'_ids': resp.inserted_ids})
    else:
        return jsonify({'msg': 'success'})

@rg_permission_api.route('/delete/<id>', methods=['DELETE'])
@admin_required
def api_delete_rg_permission(id):
    delete_rg_permission(id)
    return jsonify({'msg': 'success'})