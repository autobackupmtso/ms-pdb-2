from flask import Blueprint, request, jsonify
from flask_cors import CORS

from bson.objectid import ObjectId

from .db_interaction.form_permission import *
from apps.pdb.decorators import admin_required

form_permission_api = Blueprint('form_permission_api', 'form_permission_api', url_prefix='/api/pdb/form-permission')
CORS(form_permission_api)

@form_permission_api.route('/get/<id>', methods=['GET'])
@admin_required
def api_get_pp(id):
    form_permission = get_form_permission(id)
    return form_permission

@form_permission_api.route('/get-all', methods=['GET'])
@admin_required
def api_get_all():
    fps = get_all()
    return jsonify(fps)

@form_permission_api.route('/facet-search', methods=['POST'])
@admin_required
def api_facet_search():
    condition = request.get_json()

    fps = facet_search(condition)
    return jsonify(fps)

@form_permission_api.route('/add', methods=['POST'])
@admin_required
def api_add_form_permission():
    inp = request.get_json()
    if 'project_id' not in inp or 'role_id' not in inp or 'form_id' not in inp or 'form_permission' not in inp: 
        return jsonify({'msg': 'Some fields are missing'}), 400 

    inserted_id = add_form_permission(inp)
    return jsonify({'_id': inserted_id}) 

@form_permission_api.route('/update', methods=['PUT'])
@admin_required
def api_update_pp():
    inp = request.get_json()
    if '_id' not in inp: 
        return jsonify({'msg': '_id is missing'}), 400 

    update_form_permission(inp)
    return jsonify({'msg': 'success'})

@form_permission_api.route('/upsert-many', methods=['PUT'])
@admin_required
def api_upsert_many():
    try:
        inp = request.get_json() 

        resp = upsert_many(inp)
        if resp != None:
            return jsonify({'_ids': resp.inserted_ids})
        else:
            return jsonify({'msg': 'success'})

    except Exception as e:
        return jsonify({'msg': str(e)}), 400

@form_permission_api.route('/delete/<id>', methods=['DELETE'])
@admin_required
def api_delete_pp(id):
    delete_form_permission(id)
    return jsonify({'msg': 'success'})