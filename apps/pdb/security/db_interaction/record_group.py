from apps.db import db 
from bson.objectid import ObjectId
from pymongo import ReturnDocument


def get_record_group(_id):
    return db.record_group.find_one({'_id': ObjectId(_id)})


def get_all():
    return list(db.record_group.find())


def facet_search(condition):
    if '_id' in condition:
        _id = ObjectId(condition['_id'])
        condition['_id'] = _id
    return list(db.record_group.find(condition))


def add_record_group(inp):
    check = db.record_group.find_one({'name': inp['name']})
    if check is None:
        resp = db.record_group.insert_one(inp)
    else:
        return {'err': 'Record group name already exists'}

    # Add full permissions for admin
    admin_id = ObjectId(db.role.find_one({'name': 'admin'})['_id']) 
    db.rg_permission.insert_one({
        'role_id': admin_id, 
        'record_group_id': ObjectId(resp.inserted_id),
        'S': True, 
        'U': True, 
        'I': True, 
        'D': True
    })

    return resp


def update_record_group(inp):
    _id = ObjectId(inp.pop('_id')) 
    resp = db.record_group.find_one_and_update({'_id': _id}, {'$set': inp}, return_document=ReturnDocument.BEFORE)
    old_name = resp['name']
    new_name = inp['name']

    # update name in tables
    tables_name = [t['name'] for t in list(db.table_metadata.find({}, {'name': 1}))]
    for table_name in tables_name:
        db[table_name].update_many({'record_group': old_name}, {'$set': {'record_group': new_name}})

    return resp


def delete_record_group(_id):
    resp = db.record_group.find_one_and_delete({'_id': ObjectId(_id)})
    name = resp['name']

    # delete record group permission
    db.rg_permission.delete_many({'record_group_id': ObjectId(_id)})

    # move records to RG default
    tables_name = [t['name'] for t in list(db.table_metadata.find({}, {'name': 1}))]
    for table_name in tables_name:
        db[table_name].update_many({'record_group': name}, {'$set': {'record_group': 'default'}})

    return resp
