from apps.db import db 
from bson.objectid import ObjectId
from pymongo import ReturnDocument


def get_role(id):
    return db.role.find_one({'_id': ObjectId(id)})


def get_all():
    return list(db.role.find())


def add_role(inp):
    check = db.role.find_one({'name': inp['name']})
    if check is None:
        resp = db.role.insert_one(inp)
        return resp
    else: 
        return {'err': 'Role name already exists'}


def update_role(inp):
    _id = ObjectId(inp.pop('_id'))
    new_name = inp['name']
    resp = db.role.find_one_and_update({'_id': _id}, {'$set': inp}, return_document=ReturnDocument.BEFORE)
    old_name = resp['name']

    # update role name in user info
    db.pdb_user.update_many({'roles': old_name}, {'$set': {'roles.$': new_name}})
    return resp


def delete_role(_id):
    role = db.role.find_one({'_id': ObjectId(_id)})
    if role['name'] == 'admin':
        return 'Unable to delete admin'
    resp = db.role.delete_one({'_id': ObjectId(_id)})
    return resp
