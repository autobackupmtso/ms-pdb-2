from apps.db import db 
from apps.security import bcrypt
from bson.objectid import ObjectId
from flask_jwt_extended import create_refresh_token

def get_info(user_id):
    return db.pdb_user.find_one({'_id': ObjectId(user_id)})

def get_user(username):
    return db.pdb_user.find_one({'username': username})

def get_all():
    return list(db.pdb_user.find())

def login_user(username, access_token):
    db.sessions.update_one({'username': username}, {'$set': {'access_token': access_token}}, upsert=True) 

def add_user(username, email, name, password_hash, roles=[]):
    u = get_user(username)
    if u is None:
        resp = db.pdb_user.insert_one({
            'username': username, 
            'email': email, 
            'name': name,
            'password_hash': password_hash,
            'roles': roles
        })
        return resp.inserted_id
    else:
        return {'err': 'username already exists'}

def update_user_info(inp):
    _id = ObjectId(inp.pop('_id'))
    resp = db.pdb_user.update_one({'_id': _id}, {'$set': inp})
    return db.pdb_user.find_one({'_id': ObjectId(_id)}) 

def logout_user(username):
    resp = db.sessions.delete_one({'username': username})
    return resp 

def delete_user(id):
    resp = db.pdb_user.delete_one({'_id': ObjectId(id)})
    return resp 

def refresh_token(current_user):
    new_access_token = create_refresh_token(identity=current_user)
    username = current_user.get('username')
    db.sessions.update_one({'username': username}, {'$set': {'access_token': new_access_token}}, upsert=True) 
    return new_access_token