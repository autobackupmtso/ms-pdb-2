from flask import Flask, jsonify
from flask.json import JSONEncoder
from flask_cors import CORS
from flask_pymongo import PyMongo
from flask_bcrypt import Bcrypt
from flask_mail import Mail
import flask_excel as excel

import os 
from datetime import datetime, timedelta
from bson import ObjectId, json_util
from config import Config
from blinker import Namespace

from apps.pdb.models.project import project_api
from apps.pdb.models.form import form_api
from apps.pdb.models.data import data_api
from apps.pdb.models.table import table_api
from apps.pdb.models.export import export_api
from apps.pdb.security.role import role_api
from apps.pdb.security.record_group import record_group_api
from apps.pdb.security.form_permission import form_permission_api
from apps.pdb.security.field_permission import field_permission_api
from apps.pdb.security.record_group_permission import rg_permission_api
from apps.pdb.security.user import user_api
from apps.pdb.background_job.rule import rule_api
from apps.pdb.background_job.task import task_controller

from .jwt import jwt, add_claims
# from .change_stream import ChangeStream


class MongoJsonEncoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime("%Y-%m-%d %H:%M:%S")
        if isinstance(obj, timedelta):
            return str(obj)
        if isinstance(obj, ObjectId):
            return str(obj)
        return json_util.default(obj, json_util.CANONICAL_JSON_OPTIONS)


# change_stream = ChangeStream()

mail = Mail()


def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(Config)

    app.json_encoder = MongoJsonEncoder
    app.excel = excel
    excel.init_excel(app)
    jwt.init_app(app)
    mail.init_app(app)
    app.mail = mail

    app.app_signal = Namespace()
    app.data_changed = app.app_signal.signal('data_changed')
    app.data_changed.connect(task_controller, app)

    # change_stream.init_app(app)
    # app.change_stream.track()

    app.register_blueprint(user_api)
    app.register_blueprint(project_api)
    app.register_blueprint(form_api)
    app.register_blueprint(data_api)
    app.register_blueprint(table_api)
    app.register_blueprint(export_api)
    app.register_blueprint(record_group_api)
    app.register_blueprint(form_permission_api)
    app.register_blueprint(role_api)
    app.register_blueprint(field_permission_api)
    app.register_blueprint(rg_permission_api)
    app.register_blueprint(rule_api)

    app.config['JWT'] = jwt 
    app.config['BCRYPT'] = Bcrypt(app)
    app.config['CLAIMS_LOADER'] = add_claims
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = timedelta(days=30)

    return app
