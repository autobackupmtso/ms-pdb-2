from flask import current_app, g
from werkzeug.local import LocalProxy
from pymongo import MongoClient


def get_db():
    db = getattr(g, "_database", None)
    db_uri = current_app.config['DB_URI']
    db_name = current_app.config['DB_NAME']
    if db is None:
        db = g._database = MongoClient(db_uri, maxPoolSize=50, wtimeout=25)[db_name]

    return db


db = LocalProxy(get_db)
