from apps.factory import create_app
from apps.pdb.background_job.task import check_matching_rule

from pymongo import MongoClient
from pymongo.errors import PyMongoError
from datetime import datetime
import atexit
import pickle
import signal
import sys
import time

app = create_app()
app.app_context().push()
# resume_token = None


def load_token():
    try:
        with open('cs_resume_token', 'rb') as f:
            return pickle.load(f)
    except:
        return None


def save_token():
    with open('cs_resume_token', 'wb') as f:
        pickle.dump(resume_token, f)


atexit.register(save_token)
signal.signal(signal.SIGTERM, save_token)
signal.signal(signal.SIGINT, save_token)


class ChangeStream:
    def __init__(self, flask_app):
        db_uri = flask_app.config['DB_URI']
        db_name = flask_app.config['DB_NAME']
        self.db = MongoClient(db_uri, maxPoolSize=50, wtimeout=2500)[db_name]

    def track(self):
        global resume_token
        pipeline = [
            {
                '$match': {
                    'ns.coll': {'$nin': [
                         'action_history', 'table_relationship', 'import_template', 'form_permission', 'field_permission',
                         'rg_permission', 'project', 'form', 'rule', 'sequence', 'sessions', 'table_metadata', 'data_upload_log',
                         'columns_info', 'action_history', 'rule_group'
                    ]}
                }
            }
        ]

        try:
            if resume_token is None:
                change_stream = self.db.watch(pipeline=pipeline)
            else:
                change_stream = self.db.watch(pipeline=pipeline, resume_after=resume_token)

            for change in change_stream:
                # print(change)
                resume_token = change_stream.resume_token
                self.trigger(change)

        except PyMongoError:
            if resume_token is None:
                pass
            else:
                with self.db.watch(pipeline=pipeline, resume_after=resume_token) as change_stream:
                    for change in change_stream:
                        resume_token = change_stream.resume_token
                        self.trigger(change)

    def trigger(self, change):
        record_flag = None

        try:
            # start = time.time()
            op_type = change['operationType']
            if op_type == 'insert':
                table_name = change['ns']['coll']
                data = change['fullDocument']
                _id = data.pop('_id')
                record_flag = 'created'

                if 'created_at' in data:
                    created_at = data.pop('created_at')
                else:
                    created_at = datetime.now()

                if 'created_by' in data:
                    created_by = data.pop('created_by')
                else:
                    created_by = 'system'

                inserted_fields = list(data.keys())
                updated_fields = []
                deleted_fields = []
                # print(deleted_fields, updated_fields, inserted_fields)

                table_check = self.db.table_metadata.find_one({'name': table_name})
                if table_check is not None:
                    # print(change)
                    for k in data.keys():
                        self.db.action_history.insert_one({
                            'table_name': table_name,
                            'key_id': _id,
                            'field': k,
                            'value': data[k],
                            'time': created_at,
                            'user': created_by
                        })

                check_matching_rule(table_name, [_id], inserted_fields, updated_fields, deleted_fields, record_flag)

            elif op_type == 'update':
                table_name = change['ns']['coll']
                data = change['updateDescription']['updatedFields']
                _id = change['documentKey']['_id']
                record_flag = 'updated'
                # print(table_name, _id)

                if 'updated_at' in data:
                    updated_at = data.pop('updated_at')
                else:
                    updated_at = datetime.now()

                if 'updated_by' in data:
                    updated_by = data.pop('updated_by')
                else:
                    # updated_by = self.db.get_collection(table_name).find_one({'_id': _id}).get('updated_by', 'system')
                    updated_by = self.db[table_name].find_one({'_id': _id}).get('updated_by', 'system')

                fields = list(data.keys())
                deleted_fields = [f for f in fields if (data[f] == '' or data[f] is None)]
                fields = list(set(fields) - set(deleted_fields))
                db_updated_fields = self.db.action_history.distinct('field', {'table_name': table_name, 'key_id': _id,
                                                                              'field': {'$in': fields}})
                updated_fields = [f for f in fields if f in db_updated_fields]
                inserted_fields = list(set(fields) - set(updated_fields))
                # print(deleted_fields, updated_fields, inserted_fields)

                table_check = self.db.table_metadata.find_one({'name': table_name})
                if table_check is not None:
                    # print(change)
                    for k in data.keys():
                        self.db.action_history.insert_one({
                            'table_name': table_name,
                            'key_id': _id,
                            'field': k,
                            'value': data[k],
                            'time': updated_at,
                            'user': updated_by
                        })

                check_matching_rule(table_name, [_id], inserted_fields, updated_fields, deleted_fields, record_flag)

            elif op_type == 'delete':
                table_name = change['ns']['coll']
                table_check = self.db.table_metadata.find_one({'name': table_name})
                record_flag = 'deleted'
                # if table_check is not None:
                #     print(change)

            # end = time.time()
            # print(op_type, end - start)
        except Exception as e:
            print('Error on line {}'.format(sys.exc_info()[-1].tb_lineno), type(e).__name__, e)


resume_token = load_token()
cs = ChangeStream(app)
cs.track()
