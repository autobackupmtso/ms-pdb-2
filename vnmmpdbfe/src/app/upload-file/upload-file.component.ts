import { Component, Input, OnInit, Type, ViewChild } from '@angular/core';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {Observable, Subject, merge} from 'rxjs';
import {debounceTime, distinctUntilChanged, filter, first, map} from 'rxjs/operators';
import { FileService } from '../services/file.service'
import { FormBuilder, Validators } from '@angular/forms';
import {FlatTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';
import {DataService} from '../services/data.service';
import {SpinnerOverlayService} from '../services/spinner-overlay.service';
import {NgbModal, NgbPanelChangeEvent, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { ThemePalette } from '@angular/material';


export interface Task {
  name: string;
  completed: boolean;
  color: ThemePalette;
  subtasks?: Task[];
}
@Component({
  selector: 'app-upload-file',
  templateUrl: './upload-file.component.html',
  styleUrls: ['./upload-file.component.scss'],
  providers: [NgbModalConfig, NgbModal]
})
export class UploadFileComponent implements OnInit {
  files: any;
  constructor(
    private fileService: FileService,
    private dataService: DataService,
    private fb: FormBuilder,
    private spinnerOverlayService : SpinnerOverlayService,
    private modalService: NgbModal,
    private config: NgbModalConfig,
    ) { }
  listServer: any = ["NAS Server VietNam", "NAS Server Myanmar"];
  pathFolder: any;
  listLink : any = [];
  listGetLink: any;
  listFileUpload: any = [];
  listFileInServer: any = []
  listFile:any = [];
  listFileServer:any;
  listFilterCheck:any = [];
  UserName: any;
  Password: any;
  Path: any;

  ngOnInit() {
  }

  public formGroup = this.fb.group({
    file: [null, Validators.required]
  });
 
  private fileName;
  onSelectServer(event: any) {
    if (event == "NAS Server VietNam") {
      this.Path = "ftp://user_control@113.161.81.5:2121"
      this.UserName = "user_control"
      this.Password = "Ericsson@123"
    } 
    if (event == "NAS Server Myanmar") {
      this.Path = "ftp://SHL2@37.111.42.210:21"
      this.UserName = "SHL2"
      this.Password = "Myanmar123"
    }
  }
  onSelectFolder(event) {
    if(this.Path){
      this.pathFolder = event.target.value
    console.log(this.pathFolder)
    const json: any = {};
    json.path_dic = this.Path + this.pathFolder
    json.username = this.UserName
    json.password = this.Password
    this.fileService.postlinkFolder(json)
      .pipe(first())
      .subscribe(
        (data: any) => {
          for (let i = 0; i < data.msg.length; i++) {
          this.listFile[i] = data.msg[i]
          }
        });
        for (let i = 0; i < this.listFile.length; i++) {
          console.log(this.listFile[i])
       }
        console.log(this.listGetLink)
    }
    
  }

  public onFileChange(event) {
    const reader = new FileReader();
 
    if (event.target.files && event.target.files.length) {
      this.fileName = event.target.files[0].name;
      const [file] = event.target.files;
      this.listFileUpload = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.formGroup.patchValue({
          file: reader.result
        });
      };
    }
  }
  /* Get Link FTP */
  getLink() {
    console.log("123")
    if(this.UserName == "user_control"){
      for ( let i = 0; i < this.listFileUpload.length; i++) {
        this.listLink[i] = "ftp://user_control@113.161.81.5:2121/" + this.pathFolder + "/" + this.listFileUpload[i]
     }
      console.log(this.listLink.length)
    }
    if(this.UserName == "SHL2"){
      for ( let i = 0; i < this.listFileUpload.length; i++) {
        this.listLink[i] = "ftp://SHL2:Myanmar123@37.111.42.210/" + this.pathFolder + "/" + this.listFileUpload[i]
     }
      console.log(this.listLink.length)
    }
    

  }
   /* To copy Text from Textbox */
   copyMessage(){
    for ( let i = 0; i < this.listFileUpload.length; i++) {
      this.listGetLink = this.listGetLink + '\n' + this.listLink[i]
   }
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = this.listGetLink;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }
  
  open(content) {
    this.modalService.open(content);
  }

  task: Task = {
    name: 'Select all',
    completed: false,
    color: 'primary',
    subtasks: this.listFile

  };

  onFilterColumnAll() {
    for (let i = 0; i < this.listFile.length; i++) {
      this.listFilterCheck[i + 1] = this.listFilterCheck[0];
    }
  }

  onListSelect(){
    this.listFileUpload = []
    for (let i = 0; i < this.listFile.length; i++) {
      if (this.listFilterCheck[i + 1]) {
        this.listFileUpload.push(this.listFile[i]);
      }
    }
    console.log(this.listFileUpload.length)
  }

}
