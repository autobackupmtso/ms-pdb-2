import { Component, OnInit } from '@angular/core';
import {TableService} from '../services/table.service';
import {SpinnerOverlayService} from '../services/spinner-overlay.service';
import { first } from 'rxjs/operators';
import { Task } from '../upload-file/upload-file.component';
import {NgbModal, NgbPanelChangeEvent, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-export-data',
  templateUrl: './export-data.component.html',
  styleUrls: ['./export-data.component.scss']
})
export class ExportDataComponent implements OnInit {

  constructor(
    private tableService: TableService,
    private spinnerOverlayService: SpinnerOverlayService,
    private modalService: NgbModal,
  ) { 
  }
  listFilterTable : any = [];
  selectedTable: any;
  listTable : any = [];
  listField : any = [];
  id : any = [];
  
  ngOnInit() {
    this.tableService.getAllTable()
      .pipe(first())
      .subscribe(
        data => {
          this.listTable = data;
          this.spinnerOverlayService.hide();
        });
  }
  onAddFilterTable() {
    const newRow = {};
    this.listFilterTable.push(newRow);
  }

  getTableInfo(id: any): Promise<void> {
    return new Promise<void>(resolve => {
      this.tableService.getTableWithId(id)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.selectedTable = data;
            resolve();
          });
    });
  }
  async onSelectTable(event: any) {
    console.log(this.listTable)
    if (event) {
      this.spinnerOverlayService.show();
      this.spinnerOverlayService.hide();
    }
  }
  task: Task = {
    name: 'Select all',
    completed: false,
    color: 'primary',
    subtasks: [this.listField[this.id]]
    
  };

  open(content) {
    this.modalService.open(content);
  }
  
}
