import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pm-project',
  templateUrl: './pm-project.component.html',
  styleUrls: ['./pm-project.component.scss']
})
export class ProjectComponent implements OnInit {

  constructor() { }

  pmID:any;
  siteID:any;
  round:any;
  aspName:any;
  planDate:any;
  pmDate:any;
  aspFixDate: any;
  pmAttachFile: any;
  reportAttach: any;
  boHteName: any;
  boCheckDate: any;
  boCheckResults: any;
  boCheckComment: any;
  fmEngineer: any;
  ngOnInit() {

  }

  getValue(_pmID, _siteID, _round, _aspName, _planDate, _pmDate, _aspFixDate, _pmAttachFile){
    console.log(_pmID.value)
    console.log(_siteID.value)
    this.pmID = _pmID.value
    this.siteID = _siteID.value
    this.round = _round.value
    this.aspName = _aspName.value
    this.planDate = _planDate.value 
    this.pmDate = _pmDate.value 
    this.aspFixDate = _pmAttachFile
    
  }
}
