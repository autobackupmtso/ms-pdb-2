import { Component, OnInit } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilService} from '../../services/util.service';
import {Subject} from 'rxjs';
import {debounceTime, first} from 'rxjs/operators';
import {UserService} from '../../services/user.service';
import {RoleService} from '../../services/role.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss']
})
export class ManageUsersComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private userService: UserService,
    private roleService: RoleService,
    public utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  listUsers: any = [];
  listFilteredUser: any = [];

  listRoles: any = [];
  newUser: any = {};
  popUpId: number;
  popUpUser: any;
  listSearch: any = {};
  page = 1;
  pageSize = 5;

  ngOnInit() {
    this.setUpMessage();
    this.setListSearch();
    this.userService.getAllUser()
      .pipe(first())
      .subscribe(
        data => {
          this.listUsers = data;
          this.listFilteredUser = this.listUsers;
        });
    this.roleService.getAllRole()
      .pipe(first())
      .subscribe(
        data => {
          this.listRoles = data;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.username = '';
    this.listSearch.name = '';
    this.listSearch.email = '';
    this.listSearch.roles = '';
  }

  performFilter() {
    this.listFilteredUser = this.listUsers.filter((field: any) =>
      field.username.toLocaleLowerCase().indexOf(this.listSearch.username.toLocaleLowerCase()) !== -1 &&
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1 &&
      field.email.toLocaleLowerCase().indexOf(this.listSearch.email.toLocaleLowerCase()) !== -1
    );
  }

  onPopUp(content: any) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
  }

  onPopUpDataUser(content: any, id: any) {
    this.popUpUser = this.utilService.deepCopy(this.listFilteredUser[id]);
    this.popUpId = id;
    this.onPopUp(content);
  }

  onSaveAddUser() {
    if (!this.newUser.username) {
      this.message.next('Username must not be blanked');
      return;
    }
    if (!this.newUser.name) {
      this.message.next('Full Name must not be blanked');
      return;
    }
    if (!this.newUser.email) {
      this.message.next('Email must not be blanked');
      return;
    } else if (!this.utilService.checkEmail(this.newUser.email)) {
      this.message.next('Email not valid');
      return;
    }
    this.spinnerOverlayService.show();
    if (!this.newUser.default_password) {
      if (!this.newUser.password) {
        this.message.next('Password must not be blanked');
        return;
      }
    }
    if (this.newUser.default_password) {
      this.newUser.password = 'Welcome-1';
    }
    delete this.newUser.default_password;
    this.newUser = this.utilService.trimJson(this.newUser);
    this.userService.register(this.newUser)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.newUser._id = data._id;
          this.listUsers.push(this.newUser);
          this.performFilter();
          this.newUser = {};
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveEdit() {
    if (!this.popUpUser.username) {
      this.message.next('Username must not be blanked');
      return;
    }
    if (!this.popUpUser.name) {
      this.message.next('Full Name must not be blanked');
      return;
    }
    if (!this.popUpUser.email) {
      this.message.next('Email must not be blanked');
      return;
    } else if (!this.utilService.checkEmail(this.popUpUser.email)) {
      this.message.next('Email not valid');
      return;
    }
    this.spinnerOverlayService.show();
    this.popUpUser = this.utilService.trimJson(this.popUpUser);
    this.userService.update(this.popUpUser)
      .pipe(first())
      .subscribe(
        (data: any) => {
          const index = this.listUsers.indexOf(this.listFilteredUser[this.popUpId]);
          this.listUsers[index] = this.utilService.deepCopy(this.popUpUser);
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onDelete(id: any) {
    this.spinnerOverlayService.show();
    this.userService.delete(this.listFilteredUser[id]._id)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.listUsers = this.listUsers.filter((field: any) =>
            field._id !== this.listFilteredUser[id]._id
          );
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  getListUser() {
    return this.listFilteredUser
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
