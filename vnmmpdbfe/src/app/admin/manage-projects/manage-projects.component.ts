import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ProjectService} from '../../services/project.service';
import {UtilService} from '../../services/util.service';
import {debounceTime, first} from 'rxjs/operators';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Subject} from 'rxjs';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-manage-projects',
  templateUrl: './manage-projects.component.html',
  styleUrls: ['./manage-projects.component.scss']
})
export class ManageProjectsComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private projectService: ProjectService,
    private utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;
  page = 1;
  pageSize = 5;
  listProject: any;
  deleteProjectName: string;
  deleteId: number;
  newProject: any = {};
  popUpId: number;
  listProjectField: any = [];
  listSearch: any = {};
  listFilteredProject: any = [];

  // tslint:disable:prefer-for-of forin
  ngOnInit() {
    this.setUpMessage();
    this.setListSearch();
    this.projectService.getAllProject()
      .pipe(first())
      .subscribe(
        data => {
          this.listProject = data;
          this.listFilteredProject = this.listProject;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.id = '';
    this.listSearch.name = '';
  }

  performFilter() {
    this.listFilteredProject = this.listProject.filter((field: any) =>
      field['Project ID'].toLocaleLowerCase().indexOf(this.listSearch.id.toLocaleLowerCase()) !== -1 &&
      field['Project Name'].toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1
    );
  }

  onAddNewField() {
    const newRow: any = {};
    this.listProjectField.push(newRow);
  }

  onSaveAdd() {
    this.newProject = this.utilService.trimJson(this.newProject);
    if (!this.checkInformation(this.newProject, false)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.newProject.data = {};
    for (let i = 0; i < this.listProjectField.length; i++) {
      this.newProject.data[this.listProjectField[i].name] = this.listProjectField[i].value;
    }
    this.projectService.addNewProject(this.newProject)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.newProject._id = data._id;
          this.listProject.push(this.newProject);
          this.performFilter();
          this.newProject = {};
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveEdit() {
    if (!this.checkInformation(this.listFilteredProject[this.popUpId], true)) {
      return;
    }
    this.spinnerOverlayService.show();
    const id = this.listProject.indexOf(this.listFilteredProject[this.popUpId]);
    this.listProject[id] = this.utilService.trimJson(this.listProject[id]);
    this.listProject[id].data = {};
    for (let i = 0; i < this.listProjectField.length; i++) {
      this.listProject[id].data[this.listProjectField[i].name] = this.listProjectField[i].value;
    }
    this.projectService.updateProject(this.listProject[id])
      .pipe(first())
      .subscribe(
        data => {
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onDelete(id: any) {
    this.spinnerOverlayService.show();
    this.projectService.deleteProject(this.listFilteredProject[id]._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listProject.splice(this.listProject.indexOf(this.listFilteredProject[id]), 1);
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onDeleteProjectField(id: any) {
    this.listProjectField.splice(id, 1);
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  onPopUpAddProject(content: any, modalBig: boolean) {
    this.listProjectField = [];
    this.onPopUp(content, modalBig);
  }

  onPopUpDataProject(content: any, modalBig: boolean, id: any) {
    this.popUpId = id;
    this.listProjectField = [];
    for (const a in this.listFilteredProject[this.popUpId].data) {
      const newField: any = {};
      newField.name = a;
      newField.value = this.listFilteredProject[this.popUpId].data[a];
      this.listProjectField.push(newField);
    }
    this.onPopUp(content, modalBig);
  }

  onPopUpDelete(content: any, modalBig: boolean, id: any) {
    this.deleteId = id;
    this.deleteProjectName = this.listFilteredProject[id]['Project Name'];
    this.onPopUp(content, modalBig);
  }

  checkInformation(project: any, isEdit: boolean) {
    if (!this.utilService.checkValue(project['Project ID'])) {
      this.message.next('Project ID must not be blanked');
      return false;
    }
    if (!this.utilService.checkValue(project['Project Name'])) {
      this.message.next('Project Name must not be blanked');
      return false;
    }
    for (let i = 0; i < this.listProject.length; i++) {
      if (this.listProject.indexOf(this.listFilteredProject[this.popUpId]) === i && isEdit === true) {
        continue;
      }
      if (this.listProject[i]['Project ID'] === project['Project ID']) {
        this.message.next('Project ID existed');
        return false;
      }
      if (this.listProject[i]['Project Name'] === project['Project Name']) {
        this.message.next('Project Name existed');
        return false;
      }
    }
    return true;
  }

  getListProject() {
    return this.listFilteredProject
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
