import { Component, OnInit } from '@angular/core';
import {debounceTime, first} from 'rxjs/operators';
import {Subject} from 'rxjs';
import {ProjectService} from '../../services/project.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilService} from '../../services/util.service';
import {RecordGroupService} from '../../services/record-group.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';

@Component({
  selector: 'app-manage-record-groups',
  templateUrl: './manage-record-groups.component.html',
  styleUrls: ['./manage-record-groups.component.scss']
})
export class ManageRecordGroupsComponent implements OnInit {

  constructor(
    private modalService: NgbModal,
    private recordGroupService: RecordGroupService,
    private utilService: UtilService,
    private spinnerOverlayService: SpinnerOverlayService
  ) { }

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  listRecordGroups: any = [];
  listFilteredRecordGroup: any = [];

  newRecordGroup: any = {};
  popUpId: number;
  popUpRecordGroup: any;
  listSearch: any = {};
  page = 1;
  pageSize = 20;

  ngOnInit() {
    this.setUpMessage();
    this.setListSearch();
    this.recordGroupService.getAllRecordGroup()
      .pipe(first())
      .subscribe(
        data => {
          this.listRecordGroups = data;
          this.utilService.sortStringAsc(this.listRecordGroups, 'name');
          this.listFilteredRecordGroup = this.listRecordGroups;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
    this.listSearch.type = '';
  }

  performFilter() {
    this.listFilteredRecordGroup = this.listRecordGroups.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1
    );
  }

  onPopUp(content: any) {
    this.modalReference = this.modalService.open(content);
    this.modalReference.result.then((result) => {
    }, (reason) => {
    });
  }

  onPopUpDataRecordGroup(content: any, id: any) {
    this.popUpRecordGroup = this.utilService.deepCopy(this.listFilteredRecordGroup[id]);
    this.popUpId = id;
    this.onPopUp(content);
  }

  onSaveAddRecordGroups() {
    this.newRecordGroup = this.utilService.trimJson(this.newRecordGroup);
    if (!this.checkInformation(this.newRecordGroup, false)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.recordGroupService.addNewRecordGroup(this.newRecordGroup)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.newRecordGroup._id = data._id;
          this.listRecordGroups.push(this.newRecordGroup);
          this.performFilter();
          this.newRecordGroup = {};
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  onSaveEdit() {
    this.popUpRecordGroup = this.utilService.trimJson(this.popUpRecordGroup);
    if (!this.checkInformation(this.popUpRecordGroup, true)) {
      return;
    }
    this.spinnerOverlayService.show();
    this.recordGroupService.updateRecordGroup(this.popUpRecordGroup)
      .pipe(first())
      .subscribe(
        data => {
          const index = this.listRecordGroups.indexOf(this.listFilteredRecordGroup[this.popUpId]);
          this.listRecordGroups[index] = this.utilService.deepCopy(this.popUpRecordGroup);
          this.performFilter();
          this.spinnerOverlayService.hide();
        });
  }

  onDelete(id: any) {
    this.spinnerOverlayService.show();
    this.recordGroupService.deleteRecordGroup(this.listFilteredRecordGroup[id]._id)
      .pipe(first())
      .subscribe(
        data => {
          this.listRecordGroups = this.listRecordGroups.filter((field: any) =>
            field._id !== this.listFilteredRecordGroup[id]._id
          );
          this.performFilter();
          this.spinnerOverlayService.hide();
          this.modalReference.close();
        });
  }

  checkInformation(recordGroup: any, isEdit: boolean) {
    if (!this.utilService.checkValue(recordGroup.name)) {
      this.message.next('Record Group Name must not be blanked');
      return false;
    }
    return true;
  }

  getListRecordGroup() {
    return this.listFilteredRecordGroup
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  getCurrentId(id: any) {
    return (id + (this.page - 1) * this.pageSize);
  }
}
