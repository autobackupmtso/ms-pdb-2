import { RouterModule, Routes } from '@angular/router';
import {NgModule} from '@angular/core';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthGuardAdminService} from './services/auth-guard-admin.service';

import { DashboardComponent } from './dashboard/dashboard.component';
import { DataComponent } from './manage-data/data/data.component';
import { ManageProjectsComponent } from './admin/manage-projects/manage-projects.component';
import {ManageRecordGroupsComponent} from './admin/manage-record-groups/manage-record-groups.component';
import {ManageRolesComponent} from './admin/manage-roles/manage-roles.component';
import {LoginComponent} from './login/login.component';
import {ManageUsersComponent} from './admin/manage-users/manage-users.component';
import {ManageTablesComponent} from './admin/manage-tables/manage-tables.component';
import {ManageFormsComponent} from './admin/manage-forms/manage-forms.component';
import {ManageTemplatesComponent} from './admin/manage-templates/manage-templates.component';
import {BulkUploadComponent} from './manage-data/bulk-upload/bulk-upload.component';
import {SelectProjectComponent} from './manage-data/select-project/select-project.component';
import {DownloadFileComponent} from './manage-data/download-file/download-file.component';
import {ManagePermissionsComponent} from './admin/manage-permissions/manage-permissions.component';
import {LogBulkUploadComponent} from './manage-data/log-bulk-upload/log-bulk-upload.component';
import {ManageRulesComponent} from './admin/manage-rules/manage-rules.component';
import { ShowDataComponent } from './manage-data/show-data/show-data.component';
import { Login2Component } from './login2/login2.component';
import { UploadFileComponent } from './upload-file/upload-file.component';
import { ProjectComponent } from './pm-project/pm-project.component';
import { SelectPathComponent } from './select-path/select-path.component';
import { ExportDataComponent } from './export-data/export-data.component';
const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },

  // common
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuardService]},
  { path: 'data', component: SelectProjectComponent, canActivate: [AuthGuardService]},
  { path: 'data/:id', component: DataComponent, canActivate: [AuthGuardService]},
  { path: 'templates', component: BulkUploadComponent, canActivate: [AuthGuardService]},
  { path: 'download-file/:id', component: DownloadFileComponent, canActivate: [AuthGuardService]},
  { path: 'log-bulk-upload', component: LogBulkUploadComponent, canActivate: [AuthGuardService]},

  // admin
  { path: 'manage-tables', component: ManageTablesComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-projects', component: ManageProjectsComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-forms', component: ManageFormsComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-record-groups', component: ManageRecordGroupsComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-roles', component: ManageRolesComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-templates', component: ManageTemplatesComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-permissions', component: ManagePermissionsComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-users', component: ManageUsersComponent, canActivate: [AuthGuardAdminService]},
  { path: 'manage-rules', component: ManageRulesComponent, canActivate: [AuthGuardAdminService]},
  { path: 'show-data', component: SelectProjectComponent, canActivate: [AuthGuardService]},
  { path: 'show-data/data', component: ShowDataComponent, canActivate: [AuthGuardAdminService]},
  { path: 'show-data/:id', component: ShowDataComponent, canActivate: [AuthGuardAdminService]},
  { path: 'upload-file', component: UploadFileComponent, canActivate: [AuthGuardAdminService]},
  { path: 'export-data', component: ExportDataComponent, canActivate: [AuthGuardAdminService]},
  { path: 'pm-project', component: ProjectComponent, canActivate: [AuthGuardAdminService]},
  { path: 'select-path', component: SelectPathComponent, canActivate: [AuthGuardAdminService]},
  // login
  { path: 'login2', component: Login2Component},

  { path: '**', redirectTo: '/login2', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuardService,
    AuthGuardAdminService
  ]
})
export class AppRoutingModule { }
