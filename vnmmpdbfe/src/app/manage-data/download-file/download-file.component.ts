import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {first} from 'rxjs/operators';
import {DataService} from '../../services/data.service';

@Component({
  selector: 'app-download-file',
  templateUrl: './download-file.component.html',
  styleUrls: ['./download-file.component.scss']
})
export class DownloadFileComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService
  ) { }

  ngOnInit() {
    this.dataService.onDownloadFileData(this.route.snapshot.paramMap.get('id'))
      .pipe(first())
      .subscribe(
        (data: any) => {
        });
  }

}
