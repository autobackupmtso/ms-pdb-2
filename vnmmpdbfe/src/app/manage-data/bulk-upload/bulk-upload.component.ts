import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataService} from '../../services/data.service';
import {debounceTime, first} from 'rxjs/operators';
import {TableService} from '../../services/table.service';
import {SpinnerOverlayService} from '../../services/spinner-overlay.service';
import {Subject} from 'rxjs';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ExportService} from '../../services/export.service';

@Component({
  selector: 'app-bulk-upload',
  templateUrl: './bulk-upload.component.html',
  styleUrls: ['./bulk-upload.component.scss']
})
export class BulkUploadComponent implements OnInit {
  @ViewChild('modalDataLog', {static: false}) modalDataLog: ElementRef;

  constructor(
    private modalService: NgbModal,
    private dataService: DataService,
    private tableService: TableService,
    private spinnerOverlayService: SpinnerOverlayService,
    private exportService: ExportService
  ) { }
  selectedFile: File;

  private message = new Subject<string>();
  staticAlertClosed = false;
  messageContent = '';
  modalReference: any;

  active = 0;
  listTable: any = [];

  listImportTemplate: any = [];
  listExportTemplate: any = [];

  listFilteredImportTemplate: any = [];
  listFilteredExportTemplate: any = [];

  selectedTable: any;

  listSearch: any = {};
  skipBlank = true;

  popUpId: number;
  dataLog: any = {};

  // tslint:disable:prefer-for-of
  async ngOnInit() {
    this.setUpMessage();
    this.setListSearch();
    this.tableService.getAllTable()
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.listTable = data;
        });
  }

  setUpMessage() {
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this.message.subscribe(message => this.messageContent = message);
    this.message.pipe(
      debounceTime(5000)
    ).subscribe(() => this.messageContent = '');
  }

  setListSearch() {
    this.listSearch.name = '';
  }

  performFilter() {
    this.listFilteredImportTemplate = this.listImportTemplate.filter((field: any) =>
      field.name.toLocaleLowerCase().indexOf(this.listSearch.name.toLocaleLowerCase()) !== -1
    );
  }

  getTemplates(): Promise<void> {
    return new Promise<void>(resolve => {
      const json: any = {};
      json.table_id = this.selectedTable._id;
      this.dataService.searchImportTemplate(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listImportTemplate = data;
            this.listFilteredImportTemplate = this.listImportTemplate;
            resolve();
          });

      this.exportService.getExportTemplateWithCondition(json)
        .pipe(first())
        .subscribe(
          (data: any) => {
            this.listExportTemplate = data;
            this.listFilteredExportTemplate = this.listExportTemplate;
          });
    });
  }

  onPopUpData(content, modalBig, id) {
    this.popUpId = id;
    this.selectedFile = null;
    this.onPopUp(content, modalBig);
  }

  onPopUp(content: any, modalBig: boolean) {
    if (modalBig) {
      this.modalReference = this.modalService.open(content, { windowClass : 'customModalClassBig'});
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    } else {
      this.modalReference = this.modalService.open(content);
      this.modalReference.result.then((result) => {
      }, (reason) => {
      });
    }
  }

  async onSelectTable(event: any) {
    this.spinnerOverlayService.show();
    this.selectedTable = event;
    await this.getTemplates();
    this.spinnerOverlayService.hide();
    if (this.listImportTemplate.length === 0) {
      return;
    }
  }

  onDownloadTemplate(template: any, type: any) {
    this.spinnerOverlayService.show();
    const json: any = {};
    json._id = template._id;
    json.type = type;
    this.dataService.downloadNewImportTemplate(json, template.name)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  onFileChangedUpload(event: any, id: any) {
    this.selectedFile = event.target.files[0];
    this.onUploadData(id);
  }

  onUploadData(id: any) {
    this.spinnerOverlayService.show();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.dataService.onUploadData(uploadData, this.listFilteredImportTemplate[id].table_id)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.dataLog = data.upload_log;
          this.onPopUp(this.modalDataLog, true);
          this.spinnerOverlayService.hide();
        }, (error: any) => {
          if (error.upload_log) {
            this.dataLog = error.upload_log;
            this.onPopUp(this.modalDataLog, true);
            this.spinnerOverlayService.hide();
          }
        });
  }

  onFileChangedUpdate(event: any) {
    this.selectedFile = event.target.files[0];
  }

  onUpdateData(id: any) {
    this.spinnerOverlayService.show();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.dataService.onUpdateData(uploadData, this.listFilteredImportTemplate[id].table_id, this.skipBlank)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.selectedFile = null;
          this.dataLog = data.upload_log;
          this.onPopUp(this.modalDataLog, true);
          this.spinnerOverlayService.hide();
        }, (error: any) => {
          if (error.upload_log) {
            this.selectedFile = null;
            this.dataLog = error.upload_log;
            this.onPopUp(this.modalDataLog, true);
            this.spinnerOverlayService.hide();
          }
        });
  }

  onExportData(id: number, useTemplate: boolean) {
    this.spinnerOverlayService.show();
    const json: any = {};
    json.template_id = this.listFilteredExportTemplate[id]._id;
    json.use_template = useTemplate;
    this.exportService.runExportTemplate(json)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  onDownloadDeleteTemplate() {
    this.spinnerOverlayService.show();
    const json: any = {};
    json.table_id = this.selectedTable._id;
    this.dataService.downloadDeleteTemplate(json)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }

  onFileChangedDelete(event: any) {
    this.selectedFile = event.target.files[0];
    this.onDeleteData();
  }

  onDeleteData() {
    this.spinnerOverlayService.show();
    const uploadData = new FormData();
    uploadData.append('file', this.selectedFile, this.selectedFile.name);
    this.dataService.onBulkDeleteData(uploadData, this.selectedTable._id)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.dataLog = data.upload_log;
          this.onPopUp(this.modalDataLog, true);
          this.spinnerOverlayService.hide();
        }, (error: any) => {
          if (error.upload_log) {
            this.dataLog = error.upload_log;
            this.onPopUp(this.modalDataLog, true);
            this.spinnerOverlayService.hide();
          }
        });
  }

  onDownloadData(fileId: any) {
    this.spinnerOverlayService.show();
    this.dataService.onDownloadFileData(fileId)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.spinnerOverlayService.hide();
        });
  }
}
