import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from "@angular/core";
import {BehaviorSubject, Subject, Observable} from 'rxjs';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })

export class FileService {
baseUrl = environment.baseUrl + '/data';
private fileList: string[] = new Array<string>();
private fileList$: Subject<string[]> = new Subject<string[]>();
private displayLoader$: Subject<boolean> = new BehaviorSubject<boolean>(false);
    
    constructor(private http: HttpClient){}

    public isLoading(): Observable<boolean> {
        return this.displayLoader$;
    }
     
    // public upload(fileName: string, fileContent: string): void {
    //     this.displayLoader$.next(true);
    //     this.http.put( this.baseUrl + '/files', {name: fileName, content: fileContent})
    //     .pipe(finalize(() => this.displayLoader$.next(false)))
    //     .subscribe(res => {
    //       this.fileList.push(fileName);
    //       this.fileList$.next(this.fileList);
    //     }, error => {
          
    //       this.displayLoader$.next(false);
    //     });
    // }

    public upload(fileName: string, fileContent: string): void {
      this.displayLoader$.next(true);
      this.http.put( this.baseUrl + '/files', {name: fileName, content: fileContent})
      .pipe(finalize(() => this.displayLoader$.next(false)))
      .subscribe(res => {
        this.fileList.push(fileName);
        this.fileList$.next(this.fileList);
      }, error => {
        
        this.displayLoader$.next(false);
      });
  }

    public list(): Observable<string[]> {
        return this.fileList$;
    }

    getFileName() {
      return this.http.get(this.baseUrl + '/get_name_file');
    }
    postlinkFolder(json: any) {
      return this.http.post(this.baseUrl + '/get_name_file', json);
    }
}