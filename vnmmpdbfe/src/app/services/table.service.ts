import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';
import {tap} from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class TableService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/table';

  getAllTable() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getTableWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  getTableWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  getDetailTable(json: any) {
    return this.http.post(this.baseUrl + '/get-detail-tables', json);
  }

  addNewTable(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateTable(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteTable(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }

  addField(json: any) {
    return this.http.post(this.baseUrl + '/add-field', json);
  }

  updateField(json: any) {
    return this.http.put(this.baseUrl + '/update-field', json);
  }

  updateManyFields(json: any) {
    return this.http.put(this.baseUrl + '/update-many-fields', json);
  }

  deleteField(id: any) {
    return this.http.delete(this.baseUrl + `/delete-field/${id}`);
  }

  onDownloadTableTemplate() {
    return this.http.get(this.baseUrl + '/download-import-template', { responseType: 'blob'})
      .pipe(tap((data: any) => {
        const blob = new Blob([data], { type: 'application/octet-stream' });
        const downloadLink = document.createElement('a');
        downloadLink.href = window.URL.createObjectURL(blob);
        downloadLink.download = 'import_table_template.xlsx';

        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
      }));
  }

  onUploadImportTable(file: FormData, tableId: any) {
    const json = {
      table_id: tableId,
    };

    const blobOverrides = new Blob([JSON.stringify(json)], {
      type: 'application/json',
    });

    file.append('json', blobOverrides);
    return this.http.post(this.baseUrl + '/add-many-fields-by-xlsx', file);
  }
}
