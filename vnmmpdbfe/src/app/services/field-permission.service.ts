import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class FieldPermissionService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/field-permission';

  getAllFieldPermission() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getFieldPermissionWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  getFieldPermissionWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  addNewFieldPermission(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateFieldPermission(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  updateListFieldsPermission(listJson: any) {
    return this.http.put(this.baseUrl + '/upsert-many', listJson);
  }

  deleteFieldPermission(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
