import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class RuleService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/rule';

  getAllRules() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getRulesWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  getRuleWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  addNewRule(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateRule(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteRule(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }

  execRule(id: any) {
    return this.http.get(this.baseUrl + `/exec-rule/${id}`);
  }

  getAllRuleGroups() {
    return this.http.get(this.baseUrl + '/get-all-group');
  }

  getRuleGroupWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/group-facet-search', json);
  }

  getRuleGroupWithId(id: any) {
    return this.http.get(this.baseUrl + `/get-group/${id}`);
  }

  addNewRuleGroup(json: any) {
    return this.http.post(this.baseUrl + '/add-group', json);
  }

  updateRuleGroup(json: any) {
    return this.http.put(this.baseUrl + '/update-group', json);
  }

  deleteRuleGroup(id: any) {
    return this.http.delete(this.baseUrl + `/delete-group/${id}`);
  }
}
