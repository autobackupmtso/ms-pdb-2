import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class UserService {

  constructor(private http: HttpClient) {}
  baseUrl = environment.baseUrl + '/user';

  getAllUser() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getUser() {
    return this.http.get(this.baseUrl + '/get');
  }

  register(json: any) {
    return this.http.post(this.baseUrl + '/register', json);
  }

  update(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  login(json: any) {
    return this.http.post(this.baseUrl + '/login', json);
  }

  logout() {
    return this.http.get(this.baseUrl + '/logout');
  }

  delete(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
