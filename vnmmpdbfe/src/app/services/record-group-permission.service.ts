import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class RecordGroupPermissionService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/rg-permission';

  getAllRecordGroupPermission() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getRecordGroupPermissionWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  getRecordGroupPermissionWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  addNewRecordGroupPermission(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateRecordGroupPermission(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  updateListRecordGroupsPermission(listJson: any) {
    return this.http.put(this.baseUrl + '/upsert-many', listJson);
  }

  deleteRecordGroupPermission(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
