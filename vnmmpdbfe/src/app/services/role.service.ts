import { Injectable } from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod';

@Injectable({ providedIn: 'root' })
export class RoleService {

  constructor(private http: HttpClient) { }
  baseUrl = environment.baseUrl + '/role';

  getAllRole() {
    return this.http.get(this.baseUrl + '/get-all');
  }

  getRolesWithCondition(json: any) {
    return this.http.post(this.baseUrl + '/facet-search', json);
  }

  getRoleWithId(id: any) {
    return this.http.get(this.baseUrl + `/get/${id}`);
  }

  addNewRole(json: any) {
    return this.http.post(this.baseUrl + '/add', json);
  }

  updateRole(json: any) {
    return this.http.put(this.baseUrl + '/update', json);
  }

  deleteRole(id: any) {
    return this.http.delete(this.baseUrl + `/delete/${id}`);
  }
}
